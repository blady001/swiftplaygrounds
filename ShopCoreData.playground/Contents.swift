//: Playground - noun: a place where people can play

import CoreData


struct Tables {
    static let user = "user"
    static let order = "order"
    static let item = "item"
    static let category = "category"
    static let cartItem = "cartitem"
}

struct Columns {
    // user related columns
    static let username = "username"
    static let email = "email"
    static let password = "password"
    static let firstname = "firstname"
    static let lastname = "lastname"
    static let address = "address"
    
    // item related columns
    static let itemName = "itemName"
    static let itemDescription = "itemDesc"
    static let price = "price"
    static let amount = "amount"
    
    // cart item related colums
    static let cartItemAmount = "cartItemAmount"
    
    // order related columns
    static let state = "orderState"
    
    // category related columns
    static let categoryName = "categoryName"
    static let categoryDescription = "categoryDesc"
}



let model = NSManagedObjectModel()

// tables creation
let userEntity = NSEntityDescription()
userEntity.name = Tables.user

let orderEntity = NSEntityDescription()
orderEntity.name = Tables.order

let itemEntity = NSEntityDescription()
itemEntity.name = Tables.item

let cartItemEntity = NSEntityDescription()
cartItemEntity.name = Tables.cartItem

let categoryEntity = NSEntityDescription()
categoryEntity.name = Tables.category
// 00 - n do 1
//
// username attrs
//
let usernameAttribute = NSAttributeDescription()
usernameAttribute.name = Columns.username
usernameAttribute.attributeType = .stringAttributeType
usernameAttribute.isIndexed = true
//usernameAttribute.isOptional = false

let emailAttribute = NSAttributeDescription()
emailAttribute.name = Columns.email
emailAttribute.attributeType = .stringAttributeType

let passwordAttribute = NSAttributeDescription()
passwordAttribute.name = Columns.password
passwordAttribute.attributeType = .stringAttributeType

let firstnameAttribute = NSAttributeDescription()
firstnameAttribute.name = Columns.firstname
firstnameAttribute.attributeType = .stringAttributeType

let lastnameAttribute = NSAttributeDescription()
lastnameAttribute.name = Columns.lastname
lastnameAttribute.attributeType = .stringAttributeType

let addressAttribute = NSAttributeDescription()
addressAttribute.name = Columns.address
addressAttribute.attributeType = .stringAttributeType


//
// item attrs
//
let itemNameAttribute = NSAttributeDescription()
itemNameAttribute.name = Columns.itemName
itemNameAttribute.attributeType = .stringAttributeType
itemNameAttribute.isIndexed = true

let itemDescriptionAttribute = NSAttributeDescription()
itemDescriptionAttribute.name = Columns.itemDescription
itemDescriptionAttribute.attributeType = .stringAttributeType
itemDescriptionAttribute.isOptional = true

let priceAttribute = NSAttributeDescription()
priceAttribute.name = Columns.price
priceAttribute.attributeType = .decimalAttributeType

let amountAttribute = NSAttributeDescription()
amountAttribute.name = Columns.amount
amountAttribute.attributeType = .integer32AttributeType

//
// cart attrs
//
let cartItemAmountAttribute = NSAttributeDescription()
cartItemAmountAttribute.name = Columns.cartItemAmount
cartItemAmountAttribute.attributeType = .integer16AttributeType

//
// order attrs
// 
let state = NSAttributeDescription()
state.name = Columns.state
state.attributeType = .stringAttributeType
state.isIndexed = true

// category attrs
let categoryName = NSAttributeDescription()
categoryName.name = Columns.categoryName
categoryName.attributeType = .stringAttributeType

let categoryDescription = NSAttributeDescription()
categoryDescription.name = Columns.categoryDescription
categoryDescription.attributeType = .stringAttributeType
categoryDescription.isOptional = true



// 
// Defining relationships
//
struct RelationshipNames {
    static let category_id = "category_id"
    static let item_id = "item_id"
    static let user_id = "user_id"
    static let order_id = "order_id"
    static let cart_item_id = "cart_item_id"
}

// item - category
let itemCategory = NSRelationshipDescription()
let categoryItems = NSRelationshipDescription()
// order - user
let orderUser = NSRelationshipDescription()
let userOrders = NSRelationshipDescription()
// order - cart item
let orderCartItems = NSRelationshipDescription()
let cartItemOrder = NSRelationshipDescription()
// cart item -- item
let boughtItem = NSRelationshipDescription()
let cartItems = NSRelationshipDescription()


itemCategory.name = RelationshipNames.category_id
itemCategory.destinationEntity = itemEntity
itemCategory.minCount = 0
itemCategory.maxCount = 1
itemCategory.deleteRule = .nullifyDeleteRule
itemCategory.inverseRelationship = categoryItems

categoryItems.name = RelationshipNames.item_id
categoryItems.destinationEntity = categoryEntity
categoryItems.minCount = 0
categoryItems.maxCount = 0
categoryItems.deleteRule = NSDeleteRule.nullifyDeleteRule
categoryItems.inverseRelationship = itemCategory


orderUser.name = RelationshipNames.user_id
orderUser.destinationEntity = orderEntity
orderUser.minCount = 0
orderUser.maxCount = 1
orderUser.deleteRule = .nullifyDeleteRule
orderUser.inverseRelationship = userOrders

userOrders.name = RelationshipNames.order_id
userOrders.destinationEntity = userEntity
userOrders.minCount = 0
userOrders.maxCount = 0
userOrders.deleteRule = .cascadeDeleteRule
userOrders.inverseRelationship = orderUser


orderCartItems.name = RelationshipNames.cart_item_id
orderCartItems.destinationEntity = cartItemEntity
orderCartItems.minCount = 0
orderCartItems.maxCount = 0
orderCartItems.deleteRule = .cascadeDeleteRule
orderCartItems.inverseRelationship = cartItemOrder

cartItemOrder.name = RelationshipNames.order_id
cartItemOrder.destinationEntity = orderEntity
cartItemOrder.minCount = 0
cartItemOrder.maxCount = 1
cartItemOrder.deleteRule = .nullifyDeleteRule
cartItemOrder.inverseRelationship = orderCartItems


boughtItem.name = RelationshipNames.item_id
boughtItem.destinationEntity = cartItemEntity
boughtItem.minCount = 0
boughtItem.maxCount = 1
boughtItem.deleteRule = .nullifyDeleteRule
boughtItem.inverseRelationship = cartItems

cartItems.name = RelationshipNames.cart_item_id
cartItems.destinationEntity = itemEntity
cartItems.minCount = 0
cartItems.maxCount = 0
cartItems.deleteRule = .denyDeleteRule
cartItems.inverseRelationship = boughtItem

//
// properties definitions
//
userEntity.properties = [usernameAttribute, emailAttribute, passwordAttribute, firstnameAttribute, lastnameAttribute, addressAttribute]

itemEntity.properties = [itemNameAttribute, itemDescriptionAttribute, priceAttribute, amountAttribute]

cartItemEntity.properties = [cartItemAmountAttribute, cartItemOrder]

orderEntity.properties = [state, orderCartItems]

categoryEntity.properties = [categoryName, categoryDescription]

//
// adding entities to model
//

model.entities = [userEntity, itemEntity, cartItemEntity, orderEntity, categoryEntity]


//
// Persistency
//
let persistencyCoordinator = NSPersistentStoreCoordinator(managedObjectModel: model)

do {
    try persistencyCoordinator.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)
} catch {
    print("nie bangla")
}


let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
context.persistentStoreCoordinator = persistencyCoordinator


//
// saving retrieving data test
//

// sample models
let sampleUser = NSEntityDescription.insertNewObject(forEntityName: Tables.user, into: context)
sampleUser.setValue("testUser", forKey: Columns.username)
sampleUser.setValue("mail@mail.com", forKey: Columns.email)
sampleUser.setValue("pwd", forKey: Columns.password)
sampleUser.setValue("Tester", forKey: Columns.firstname)
sampleUser.setValue("Testerowicz", forKey: Columns.lastname)
sampleUser.setValue("Test address 39-400", forKey: Columns.address)

let sampleCategory = NSEntityDescription.insertNewObject(forEntityName: Tables.category, into: context)
sampleCategory.setValue("Computers", forKey: Columns.categoryName)
sampleCategory.setValue("sample desc", forKey: Columns.categoryDescription)

let sampleProduct = NSEntityDescription.insertNewObject(forEntityName: Tables.item, into: context)
sampleProduct.setValue("Super PC", forKey: Columns.itemName)
sampleProduct.setValue("the best", forKey: Columns.itemDescription)
sampleProduct.setValue(Decimal(999.99), forKey: Columns.price)
sampleProduct.setValue(2, forKey: Columns.amount)
// TODO: How do deal with relationships


let sampleCartItem = NSEntityDescription.insertNewObject(forEntityName: Tables.cartItem, into: context)
sampleCartItem.setValue(2, forKey: Columns.cartItemAmount)

let sampleOrder = NSEntityDescription.insertNewObject(forEntityName: Tables.order, into: context)
sampleOrder.setValue("pending", forKey: Columns.state)

sampleOrder.setValue(NSSet(object: sampleCartItem), forKey: RelationshipNames.cart_item_id)


do {
    try context.save()
} catch {
    print("Save nie dziala")
}

//var fetchRequest = NSFetchRequest<NSManagedObject>(entityName: Tables.cartItem)
//var results: [NSManagedObject] = []
//
//do {
//    results = try context.fetch(fetchRequest)
//} catch {
//    print("Fetch nie dziala")
//}
//
//print("rezultat, kateogorie: \(results[0].objectID)")
//
//let sampleOrder = NSEntityDescription.insertNewObject(forEntityName: Tables.order, into: context)
//sampleOrder.setValue("pending", forKey: Columns.state)
//
//
//sampleOrder.setValue(NSSet(object: results[0].objectID), forKey: RelationshipNames.cart_item_id)
//






