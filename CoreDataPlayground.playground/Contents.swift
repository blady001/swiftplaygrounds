//: Playground - noun: a place where people can play

import CoreData

//struct Tables {
//    var products = "products"
//    var categories = "categories"
//}
//
//struct Columns {
//    
//}
//
//struct Errors {
//    
//}

let model = NSManagedObjectModel()

// Table creation
let productsEntity = NSEntityDescription()
productsEntity.name = "products"

let categoriesEntity = NSEntityDescription()
categoriesEntity.name = "categories"


// Describe single attribute
let nameAttribute = NSAttributeDescription()
nameAttribute.name = "name"
nameAttribute.attributeType = NSAttributeType.stringAttributeType
nameAttribute.isIndexed = false
nameAttribute.isOptional = false

// name is the same for categories and products



let descriptionAttribute = NSAttributeDescription()
descriptionAttribute.name = "desc"
descriptionAttribute.attributeType = NSAttributeType.stringAttributeType
descriptionAttribute.isIndexed = false
descriptionAttribute.isOptional = true

let priceAttribute = NSAttributeDescription()
priceAttribute.name = "price"
priceAttribute.attributeType = NSAttributeType.decimalAttributeType
priceAttribute.isIndexed = false
priceAttribute.isOptional = false

//
// Defining relations
let categoriesRelationship = NSRelationshipDescription()
let productsRelationship = NSRelationshipDescription()

// productsRelationDefinition
productsRelationship.name = "category_id"
productsRelationship.destinationEntity = productsEntity
productsRelationship.minCount = 0
productsRelationship.maxCount = 0 // 00 - 01 1 - 1
// productsRelationship.deleteRule = NSDeleteRule.cascadeDeleteRule
productsRelationship.inverseRelationship = categoriesRelationship

// categories relation definition
categoriesRelationship.name = "product_id"
categoriesRelationship.destinationEntity = categoriesEntity
categoriesRelationship.minCount = 0
categoriesRelationship.maxCount = 0
categoriesRelationship.deleteRule = NSDeleteRule.cascadeDeleteRule
categoriesRelationship.inverseRelationship = productsRelationship


productsEntity.properties = [priceAttribute, nameAttribute, descriptionAttribute, categoriesRelationship]
categoriesEntity.properties = [nameAttribute, descriptionAttribute].map{ return ($0.copy() as! NSPropertyDescription)}
//categoriesEntity.properties = [nameAttribute.copy() as! NSPropertyDescription, descriptionAttribute.copy() as! NSPropertyDescription]

// wtf is $0 - it stands for an argument that comes in, this should be possible to be rewritten as:
// categoriesEntity.properties = [nameAttribute, descriptionAttribute].map { attr in return attr.copy() as! NSPropertyDescription }


model.entities = [productsEntity, categoriesEntity]

// cos do persystencji - w jaki sposob przechowujemy dane (H2? - domyslnie nie jest persistent)
let persistencyCoordinator = NSPersistentStoreCoordinator(managedObjectModel: model)

do {
    try persistencyCoordinator.addPersistentStore(ofType: NSInMemoryStoreType, configurationName: nil, at: nil, options: nil)
} catch {
    print("nie bangla xd")
}

// zeby dobrac sie do kontekstu + FAJNY MOTYW Z KROPKA JESLI TYPY SIE ZGADZAJA
let context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
context.persistentStoreCoordinator = persistencyCoordinator

//
// No to juz mozemy dodawac i zapisywac dane
// ########
//

// Utworzenie i zapisanie obiektu
let product = NSEntityDescription.insertNewObject(forEntityName: "products", into: context)

product.setValue("chleb razowy", forKey: "name")
product.setValue(3, forKey: "price")
product.setValue("na zakwasie", forKey: "desc")

let category = NSEntityDescription.insertNewObject(forEntityName: "categories", into: context)
category.setValue("pieczywo", forKey: "name")
category.setValue("świeże pieczywo", forKey: "desc")

// musimy zapisac kontekst
do {
    try context.save()
} catch {
    print("kontekst nie dziala")
}

//----
// fetch request zeby sprawdzic czy dziala
////////
var fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "products")
// fetchRequest.predicate = NSPredicate(format: "products", 1) // w zasadzie niepotrzebne

var results: [NSManagedObject] = []

do {
    results = try context.fetch(fetchRequest)
} catch {
    print("Nie dziala fetch")
}

print("rezultaty: \(results.count)")


// //////////
// //////////
// CUSTOM DB
// //////////


struct Tables {
    static let user = "user"
    static let order = "order"
    static let item = "item"
    static let category = "category"
}

struct Columns {
    // user related columns
    static let username = "username"
    static let email = "email"
    static let password = "password"
    static let firstname = "firstname"
    static let lastname = "lastname"
    static let address = "address"
    
    // item related columns
    static let itemName = "itemName"
    static let itemDescription = "itemDesc"
    static let price = "price"
    static let amount = "amount"
    
    // order related columns
    static let state = "orderState"
    
    // category related columns
    static let categoryName = "categoryName"
    static let categoryDescription = "categoryDesc"
}

// let model - NSManagedObjectModel()

// tables creation
let userEntity = NSEntityDescription()
userEntity.name = Tables.user

let orderEntity = NSEntityDescription()
orderEntity.name = Tables.order

let itemEntity = NSEntityDescription()
itemEntity.name = Tables.order

let categoryEntity = NSEntityDescription()
categoryEntity.name = Tables.category

// username attrs
let usernameAttribute = NSAttributeDescription()
usernameAttribute.name = Columns.username
usernameAttribute.attributeType = .stringAttributeType
usernameAttribute.isIndexed = true
//usernameAttribute.isOptional = false

let emailAttribute = NSAttributeDescription()
emailAttribute.name = Columns.email
emailAttribute.attributeType = .stringAttributeType

let passwordAttribute = NSAttributeDescription()
passwordAttribute.name = Columns.password
passwordAttribute.attributeType = .stringAttributeType

let firstnameAttribute = NSAttributeDescription()
firstnameAttribute.name = Columns.firstname
firstnameAttribute.attributeType = .stringAttributeType

let lastnameAttribute = NSAttributeDescription()
lastnameAttribute.name = Columns.lastname
lastnameAttribute.attributeType = .stringAttributeType



